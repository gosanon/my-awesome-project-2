# Task 2
## Help message
```python
python main.py -h
```
OR
```python
python main.py --help
```
## Example usage
```
python main.py
```
*(no terminal-level parametrization)*
